import java.util.ArrayList;

/**
 *  Authors: MRB5
 */

public class Library {
    private ArrayList<Item> item_list; 

    public Library() {
        item_list = new ArrayList<Item>();
    }

    public void addItem(Item i) { 
        this.item_list.add(i);
    }

    public void removeItem(Item i) {
        this.item_list.remove(i);
    }

    public void borrowItem(Item i) {
        if (!this.item_list.contains(i)) {
            System.out.println("Sorry, the item you want to borrow is not available.");
            return;
        }

        if (i instanceof Borrowable) {
            if (i.getQuantity() > 0)    i.setQuantity(i.getQuantity() - 1);
            else    System.out.println("Sorry, this item is out of stock.");
        } else {
            System.out.println("Sorry, periodical are not for borrowing.");
        }
    }

    public void returnItem(Item i) {
        if (i instanceof Borrowable) 
            i.setQuantity(i.getQuantity() + 1);
        else
            System.out.println("Sorry, you cannot return this item.");
    }

    public ArrayList<Item> getItems() {
        return this.item_list;
    }

    public static void main(String[] args) {
        Library lib = new Library();

        Item book1 = new Book("book1", "author1", 5.99, 200);
        Item cd1 = new CD("cd1", "artist1", 19.99, 150);
        Item p1 = new Periodical("p1", "publisher1", 15.99, 100);

        lib.addItem(book1);
        lib.addItem(cd1);
        lib.addItem(p1);

        // borrow Periodical and return it
        lib.borrowItem(p1);
        System.out.println(lib.getItems());
        lib.returnItem(p1);
        System.out.println(lib.getItems());

        // borrow Book and return it
        lib.borrowItem(book1);
        System.out.println(lib.getItems());
        lib.returnItem(book1);
        System.out.println(lib.getItems());
    }
}